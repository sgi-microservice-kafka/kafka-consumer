package com.sgi.consumer;

import org.apache.kafka.clients.admin.NewTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaOperations;
import org.springframework.kafka.listener.CommonErrorHandler;
import org.springframework.kafka.listener.DeadLetterPublishingRecoverer;
import org.springframework.kafka.listener.DefaultErrorHandler;
import org.springframework.kafka.support.converter.JsonMessageConverter;
import org.springframework.kafka.support.converter.RecordMessageConverter;
import org.springframework.util.backoff.FixedBackOff;

@SpringBootApplication
public class ConsumerApplication {
	private final Logger logger = LoggerFactory.getLogger(ConsumerApplication.class);
	private final TaskExecutor exec = new SimpleAsyncTaskExecutor();
	public static void main(String[] args) {
		SpringApplication.run(ConsumerApplication.class, args);
	}

	/*
	 * Boot will autowire this into the container factory.
	 */
//	@Bean
//	public CommonErrorHandler errorHandler(KafkaOperations<Object, Object> template) {
//		return new DefaultErrorHandler(
//				new DeadLetterPublishingRecoverer(template), new FixedBackOff(1000L, 2));
//	}
//
	@Bean
	public RecordMessageConverter converter() {
		return new JsonMessageConverter();
	}
}
