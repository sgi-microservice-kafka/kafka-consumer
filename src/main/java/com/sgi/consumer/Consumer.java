package com.sgi.consumer;

import com.sgi.consumer.common.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.core.env.Environment;

@Component
public class Consumer {
    @Autowired
    private KafkaTemplate<Object, Object> template;

    @KafkaListener(topics = {"${currenttopic}"})
    public void processMessage(Order newOrder) {
        System.out.println("New order received: " + newOrder.toString());
    }
}
