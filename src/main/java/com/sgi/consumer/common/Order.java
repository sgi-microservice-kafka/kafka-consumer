package com.sgi.consumer.common;

/**
 * @author Gary Russell
 * @since 2.2.1
 *
 */
public class Order {

    private int customerId;
    private String cardNumber;
    private int productId;
    private int amount;

    public Order(){
    }

    public Order(int customerId, String cardNumber, int productId, int amount) {
        this.customerId = customerId;
        this.cardNumber = cardNumber;
        this.productId = productId;
        this.amount= amount;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String toString(){
        return ""+this.customerId+"-"+this.cardNumber+"-"+this.productId+" "+this.amount;
    }
}